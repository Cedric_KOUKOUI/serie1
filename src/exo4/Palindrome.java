package exo4;

public class Palindrome {
	public boolean palindrome(String chaine) {

		String newChaine = chaine.replaceAll(" ", "").toLowerCase();

		int i = 0, j = newChaine.length();

		while (i < newChaine.length()) {
			if (newChaine.charAt(i) != newChaine.charAt(j - 1))
				return false;
			i++;
			j--;
		}
		return true;

	}
}
