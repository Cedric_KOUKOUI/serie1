package exo2;
import java.math.BigInteger;

public class Factorielle {
	private int nombre;
	 public int intFactorielle (int nbr) {
	        int f = 1;
	        for (int i=1; i<=nbr; i++) f=f*i;
	        return(f);
	 }
	 public double doubleFactorielle (int nbr) {
	        double f = (int)nbr;
	        for (int i=1; i<=nbr; i++) f=f*i;
	        return(f);
	 }
	 public BigInteger bigIntFactorielle (int nbr) {
		 BigInteger f = BigInteger.valueOf(nbr);
		 for (int i=1; i<=nbr; i++)
			 f= f.multiply(BigInteger.valueOf(i));
		 return(f);
	 }

}

