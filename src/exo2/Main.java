package exo2;
import exo2.Factorielle;

public class Main {
	public static void main (String[] args) {
		Factorielle factint =  new Factorielle();
		Factorielle factdo =  new Factorielle();
		Factorielle factBigint =  new Factorielle();
		System.out.println("Factorielle de 10 avec intFactorielle = "+factint.intFactorielle(10));
		System.out.println("Factorielle de 10 avec doubleFactorielle = "+factdo.doubleFactorielle(10));
		System.out.println("Factorielle de 10 avec bigIntFactorielle = "+factBigint.bigIntFactorielle(10));
		
	}
}
